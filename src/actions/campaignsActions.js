import * as types from './camapignsRuducersTypes';

const BASE_URL = 'https://5cd3f999-f49f-4e42-8b8b-173c7185f093.mock.pstmn.io';

const getCampaignsFromSourceSDK = () => fetch(`${BASE_URL}/campaigns`)
    .then(result => result.json())
    .then(json => {
        if (Array.isArray(json)) {
            return json;
        }
        return [];
    });

const getStatsByCampaignIdSDK = (campaignId) => {
    const GET_STATS_URL = `${BASE_URL}/campaigns/${campaignId}/stats`;
    return fetch(GET_STATS_URL)
        .then(result => result.json())
        .then(json => {
            if (Array.isArray(json)) {
                return json;
            }
            return [];
        });
};

const deactivateCampaignSDK = (campaignId) => {
    const DEACTIVATE_URL = `${BASE_URL}/campaigns/${campaignId}/deactivate`;
    return fetch(DEACTIVATE_URL, { method: 'POST' })
        .then(result => result.json());
};

const activateCampaignSDK = (campaignId) => {
    const ACTIVATE_URL = `${BASE_URL}/campaigns/${campaignId}/activate`;
    return fetch(ACTIVATE_URL, { method: 'POST' })
        .then(result => result.json());
};


const getAllCampaigns = () => ({
    type: types.GET_ALL_CAMPAIGNS,
    promise: getCampaignsFromSourceSDK(),
});

const getAllStatsForCampaignById = (campaignId) => ({
    type: types.GET_STATS_BY_CAMPAIGN,
    campaignId,
    promise: getStatsByCampaignIdSDK(campaignId),
});

const activateCampaign = (campaignId) => ({
    type: types.ACTIVATE_CAMPAIGN,
    campaignId,
    promise: activateCampaignSDK(campaignId),
});

const deactivateCampaign = (campaignId) => ({
    type: types.DEACTIVATE_CAMPAIGN,
    campaignId,
    promise: deactivateCampaignSDK(campaignId),
});

export {
    getAllCampaigns,
    getAllStatsForCampaignById,
    deactivateCampaign,
    activateCampaign,
};
