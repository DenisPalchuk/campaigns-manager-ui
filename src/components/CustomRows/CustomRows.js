import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import * as actions from '../../actions/campaignsActions';

const statusEnum = [{ status: 'ACTIVE', changeLabel: 'Deactivate' },
    { status: 'INACTIVE', changeLabel: 'Activate' }];

class CustomRows extends Component {

    static propTypes = {
        id: React.PropTypes.string.isRequired,
        name: React.PropTypes.string.isRequired,
        dailyBudget: React.PropTypes.number.isRequired,
        totalBudget: React.PropTypes.number.isRequired,
        status: React.PropTypes.oneOf(statusEnum.map(statEn => statEn.status)),
        activateCampaign: React.PropTypes.func.isRequired,
        deactivateCampaign: React.PropTypes.func.isRequired,
    };

    static getChangeStatusLabel(currentStatus) {
        const index = statusEnum.map(status => status.status)
            .indexOf(currentStatus);
        if (index === -1) {
            return 'Wrong Status';
        }
        return statusEnum[index].changeLabel;
    }

    constructor(props) {
        super(props);
        this.changeStatus = this.changeStatus.bind(this);
    }

    changeStatus() {
        const changeStatusMethod =
            this.statusIsActive() ? 'deactivateCampaign' : 'activateCampaign';
        this.props[changeStatusMethod](this.props.id);
    }

    statusIsActive() {
        return this.props.status === 'ACTIVE';
    }

    render() {
        const statusClass = `badge ${this.statusIsActive() ? 'badge-primary' : 'badge-secondary'}`;

        return (
          <tr>
            <td><span
              className={statusClass}>{this.props.status}</span>
            </td>
            <td>{this.props.name}</td>
            <td>{this.props.dailyBudget}</td>
            <td>{this.props.totalBudget}</td>
            <td>
              <div className="dropdown show">
                <button
                  className="btn btn-secondary btn-sm dropdown-toggle"
                  type="button"
                  data-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false">Actions
                        </button>

                <div className="dropdown-menu" aria-labelledby="dropdownMenuLink">
                  <button className="dropdown-item" onClick={this.changeStatus}>
                    {CustomRows.getChangeStatusLabel(this.props.status)}
                  </button>

                  <Link className="dropdown-item" to={`/campaign/${this.props.id}`}>
                      Stats
                  </Link>
                </div>
              </div>
            </td>
          </tr>
        );
    }
}

const mapDispatchToProps = (dispatch) => ({
    activateCampaign:
        (campaignId) => dispatch(actions.activateCampaign(campaignId)),
    deactivateCampaign:
        (campaignId) => dispatch(actions.deactivateCampaign(campaignId)),
});

export default connect(undefined, mapDispatchToProps)(CustomRows);

