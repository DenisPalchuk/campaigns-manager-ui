import 'whatwg-fetch';
import { createStore, applyMiddleware } from 'redux';
import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';
import Header from '../Header/Header';
import CampaignsTable from '../CampaignsTable/CampaignsTable';
import * as actions from '../../actions/campaignsActions';
import campaignReducer from '../../reducers/campaignReducer';
import promiseMiddleware from '../../middlewares/promiseMiddleware';
import CampaignStat from '../CampaignStat/CampaignStat';

const store = applyMiddleware(promiseMiddleware)(createStore)(campaignReducer);

class App extends Component {
    componentDidMount() {
        store.dispatch(actions.getAllCampaigns());
    }

    render() {
        return (
          <Provider store={store}>
            <div className="app">
              <Header />
              <Switch>
                <Route exact path="/" component={CampaignsTable} />
                <Route component={CampaignStat} path="/campaign/:campaignId" />
              </Switch>
            </div>
          </Provider>
        );
    }
}

export default App;
