import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link, Redirect } from 'react-router-dom';
import shortid from 'shortid';
import * as actions from '../../actions/campaignsActions';
import CampaignGraph from '../CampaignGraph/CampaignGraph';

const renderChangeCampaignItems = (campaigns) => campaigns.map(campaign => (
  <Link
    key={shortid.generate()}
    className="dropdown-item"
    to={`/campaign/${campaign.id}`}>{campaign.name}
  </Link>
));

class CampaignStat extends Component {
    static propTypes = {
        campaigns: React.PropTypes.array.isRequired,
        match: React.PropTypes.any,
        getAllStatsForCampaignById: React.PropTypes.func,
        history: React.PropTypes.object,
    };

    componentDidMount() {
        this.props
            .getAllStatsForCampaignById(this.props.match.params.campaignId);
    }

    componentDidUpdate(prevProps) {
        const params = this.props.match.params;
        if (params.campaignId !== prevProps.match.params.campaignId) {
            this.props.getAllStatsForCampaignById(params.campaignId);
        }
    }

    getCampaignById(campaignId) {
        return this.props.campaigns
            .find(campaign => campaign.id === campaignId);
    }

    getOtherCampaigns(exceptCampaign) {
        return this.props.campaigns
            .filter(campaign => campaign !== exceptCampaign);
    }

    render() {
        if (!this.props.campaigns.length) {
            return <Redirect to="/" />;
        }

        const camp = this.getCampaignById(this.props.match.params.campaignId);
        const otherCampaigns = this.getOtherCampaigns(camp);
        const changeCampaignLinks = renderChangeCampaignItems(otherCampaigns);
        let graphPlaceholder = '';
        if (camp && camp.hasOwnProperty('stats')) {
            graphPlaceholder = (
              <div className="row">
                <CampaignGraph stats={camp.stats} />
              </div>
            );
        }

        return (
          <div className="container">
            <div className="row">
              <div className="col">
                <h2>{camp.name}</h2>
              </div>

              <div className="col">
                <div className="btn-group">
                  <button
                    type="button"
                    className="btn btn-secondary dropdown-toggle"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false">Change Campaign
                            </button>
                  <div className="dropdown-menu">
                    {changeCampaignLinks}
                  </div>
                </div>

                <Link to={'/'} type="button" className="btn btn-light">All
                            Campaigns
                        </Link>
              </div>

            </div>
            {graphPlaceholder}
          </div>
        );
    }
}

const mapStateToProps = (state) => ({
    campaigns: state.campaigns,
});

const mapDispatchToProps = (dispatch) => ({
    getAllStatsForCampaignById: (campaignId) => dispatch(actions.getAllStatsForCampaignById(campaignId)),
});

const connectedCampaignsStat =
    connect(mapStateToProps, mapDispatchToProps)(CampaignStat);
export default connectedCampaignsStat;
