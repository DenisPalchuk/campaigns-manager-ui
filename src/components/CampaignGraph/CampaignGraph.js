import React, { Component } from 'react';
import ReactHighcharts from 'react-highcharts';

const graphConfig = {
    chart: {
        type: 'spline',
        width: '1000',
    },
    xAxis: {
        categories: [],
    },
    yAxis: {
        title: {
            text: 'Impressions',
        },
    },
    series: [{
        name: 'Impression',
        data: [],
    }],
    title: {
        text: false,
    },
    legend: {
        enabled: false,
    },
    credits: false,
};

class CampaignGraph extends Component {
    static propTypes = {
        stats: React.PropTypes.array.isRequired,
    };

    componentWillMount() {
        this.config = graphConfig;
    }

    render() {
        this.props.stats.forEach(item => {
            this.config.series[0].data.push(item.impressions);
            this.config.xAxis.categories.push(item.date);
        });

        return (
          <ReactHighcharts config={this.config} />
        );
    }
}

export default CampaignGraph;
