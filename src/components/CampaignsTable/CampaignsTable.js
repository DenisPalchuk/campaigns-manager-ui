import React, { Component } from 'react';
import shortid from 'shortid';
import { connect } from 'react-redux';
import CustomRow from '../CustomRows/CustomRows';

const renderCampaigns = (campaigns) => {
    if (!campaigns.length) {
        return [];
    }

    return campaigns
        .filter(campaign => campaign.name &&
            campaign.daily_budget &&
            campaign.total_budget)
        .map(campaign => (
          <CustomRow
            key={shortid.generate()}
            name={campaign.name}
            dailyBudget={campaign.daily_budget}
            totalBudget={campaign.total_budget}
            status={campaign.status}
            id={campaign.id}
            />
        ));
};


class CampaignsTable extends Component {
    static propTypes = {
        campaigns: React.PropTypes.array.isRequired,
    };

    render() {
        const customRows = renderCampaigns(this.props.campaigns);

        return (
          <div className="container">
            <div className="row">
              <table className="table table-bordered">
                <thead>
                  <tr>
                    <th>Status</th>
                    <th>Name</th>
                    <th>Daily Budget</th>
                    <th>Total Budget</th>
                    <th />
                  </tr>
                </thead>
                <tbody>
                  {customRows}
                </tbody>
              </table>
            </div>
          </div>
        );
    }
}

const mapStateToProps = (state) => ({
    campaigns: state.campaigns,
});

const connectedCampaigns = connect(mapStateToProps)(CampaignsTable);

export default connectedCampaigns;
