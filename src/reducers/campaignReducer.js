import * as types from '../actions/camapignsRuducersTypes';

export default function newsReducer(state = { campaigns: [] }, action) {
    switch (action.type) {
    case types.GET_ALL_CAMPAIGNS: {
        if (action.res.length) {
            return ({
                campaigns: action.res,
            });
        }
        break;
    }

    case types.GET_STATS_BY_CAMPAIGN: {
        if (action.res.length) {
            const extendedCampaign = state.campaigns
                .find(campaign => campaign.id === action.campaignId);
            if (extendedCampaign) {
                extendedCampaign.stats = action.res;
            }
            return ({
                campaigns: state.campaigns.slice(0),
            });
        }
        break;
    }

    case types.ACTIVATE_CAMPAIGN: {
        if (action.res) {
            const filteredCampaign = state.campaigns
                    .find(campaign => campaign.id === action.campaignId);
            if (filteredCampaign) {
                filteredCampaign.status = 'ACTIVE';
            }
            return ({
                campaigns: state.campaigns.slice(0),
            });
        }
        break;
    }

    case types.DEACTIVATE_CAMPAIGN: {
        if (action.res) {
            const filteredCampaign = state.campaigns
                    .find(campaign => campaign.id === action.campaignId);
            if (filteredCampaign) {
                filteredCampaign.status = 'INACTIVE';
            }
            return ({
                campaigns: state.campaigns.slice(0),
            });
        }
        break;
    }
    default:
    }
    return state;
}
